package com.example.frsyDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrsyDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrsyDemoApplication.class, args);
	}

}
